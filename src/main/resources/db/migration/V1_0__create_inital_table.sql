create table if not exists books(
    id INTEGER NOT NULL AUTO_INCREMENT,
    book_name varchar(30) NOT NULL,
    isbn varchar(30) NOT NULL,
    author varchar(30) NOT NULL,
    price varchar(30) NOT NULL,
    stock INTEGER NOT NULL,
    PRIMARY KEY (id)
);

create table if not exists customer(
    id INTEGER NOT NULL AUTO_INCREMENT,
    customer_email varchar(225) NOT NULL,
    first_name varchar(225) NOT NULL,
    last_name varchar(225) NOT NULL,
    phone_number varchar(225) NOT NULL,
    customer_address varchar(225) NOT NULL,
    PRIMARY KEY (id)
);