package com.mainservice.messaging;

import com.mainservice.shared.LogMessage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class BookLogMessagingService {

    private final RabbitTemplate template;

    public void convertAndSend(LogMessage logMessage){
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logMessage);
    }
}
