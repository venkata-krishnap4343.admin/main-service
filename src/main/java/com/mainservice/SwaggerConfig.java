package com.mainservice;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Component
@EnableSwagger2
public class SwaggerConfig{

    @Bean
    public Docket books() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Books")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.mainservice.book"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket customer() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("Customer")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.mainservice.customer"))
                .paths(PathSelectors.any())
                .build();
    }



}