package com.mainservice.book;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mainservice.messaging.BookLogMessagingService;
import com.mainservice.shared.LogMessage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class BookService {

    BookDao bookDao;
    BookLogMessagingService bookLogMessagingService;

    public Book addNewBook(Book book) {
        BookEntity verifyBook = bookDao.findByIsbn(book.getIsbn());
        if(verifyBook == null) {
            BookEntity savedBooks = bookDao.save(BookEntity.builder()
                    .author(book.getAuthor())
                    .bookName(book.getBookName())
                    .isbn(book.getIsbn())
                    .price(book.getPrice())
                    .stock(book.getStock())
                    .build());
            bookLogMessagingService.convertAndSend(LogMessage.builder().message(savedBooks.toString()).time(new Date().toString()).build());

            return Book.builder()
                    .author(savedBooks.getAuthor())
                    .bookName(savedBooks.getBookName())
                    .isbn(savedBooks.getIsbn())
                    .price(savedBooks.getPrice())
                    .stock(savedBooks.getStock())
                    .build();
        } else {
            throw new RuntimeException("Book Already Exists");
        }
    }

    public Book getBookWithBookName(String bookName) {
        BookEntity bookEntity = bookDao.findByBookName(bookName);
        if(bookEntity != null){
            return Book.builder().bookName(bookEntity.getBookName()).price(bookEntity.getPrice()).isbn(bookEntity.getIsbn()).author(bookEntity.getAuthor()).stock(bookEntity.getStock()).build();
        } else{
            throw new RuntimeException("Book cannot be found");
        }
    }

    public List<Book> getBooksWithAuthorName(String author) {
        List<BookEntity> bookEntity = bookDao.findByAuthor(author);

        if(!bookEntity.isEmpty()){
            return bookEntity.stream().map(e->Book.builder()
                    .author(e.getAuthor())
                    .bookName(e.getBookName())
                    .stock(e.getStock())
                    .isbn(e.getIsbn())
                    .price(e.getPrice())
                    .build()).collect(Collectors.toList());
        }else {
            throw new RuntimeException("No books found");
        }
    }
}
