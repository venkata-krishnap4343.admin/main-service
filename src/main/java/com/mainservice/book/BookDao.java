package com.mainservice.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookDao extends JpaRepository<BookEntity, Integer> {
    BookEntity findByIsbn(String isbn);

    BookEntity findByBookName(String bookName);

    List<BookEntity> findByAuthor(String author);
}
