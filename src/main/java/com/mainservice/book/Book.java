package com.mainservice.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book {
    private String bookName;
    private String isbn;
    private String author;
    private String price;
    private Integer stock;
}
