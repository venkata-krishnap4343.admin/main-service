package com.mainservice.book;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/book/service")
@AllArgsConstructor
public class BookController {

    BookService bookService;

    @PostMapping(path = "/add-new-book")
    public Book addNewBook(@RequestBody Book book) {
        return bookService.addNewBook(book);
    }

    @GetMapping(path = "/bookName/get-book")
    public Book getBookWithBookName(@RequestParam("bookName") String bookName){
        return bookService.getBookWithBookName(bookName);
    }

    @GetMapping(path = "/author/get-book")
    public List<Book> getBooksWithAuthorName(@RequestParam("author") String author){
        return bookService.getBooksWithAuthorName(author);
    }
}
