package com.mainservice.customer;

import com.mainservice.messaging.CustomerLogMessagingService;
import com.mainservice.shared.LogMessage;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@AllArgsConstructor
public class CustomerService {

    CustomerDao customerDao;
    CustomerLogMessagingService customerLogMessagingService;

    public Customer addNewCustomer(Customer customer) {
        CustomerEntity checkCustomer = customerDao.findByCustomerEmail(customer.getCustomerEmail());
        if(checkCustomer == null) {
            CustomerEntity savedCustomer = customerDao.save(CustomerEntity.builder()
                    .customerEmail(customer.getCustomerEmail())
                    .firstName(customer.getFirstName())
                    .lastName(customer.getLastName())
                    .phoneNumber(customer.getPhoneNumber())
                    .customerAddress(customer.getCustomerAddress())
                    .build());

            customerLogMessagingService.convertAndSend(LogMessage.builder().message(savedCustomer.toString()).time(new Date().toString()).build());

            return Customer.builder()
                    .customerEmail(savedCustomer.getCustomerEmail())
                    .firstName(savedCustomer.getFirstName())
                    .lastName(savedCustomer.getLastName())
                    .phoneNumber(savedCustomer.getPhoneNumber())
                    .customerAddress(savedCustomer.getCustomerAddress())
                    .build();
        }else{
            throw new RuntimeException("User already exists");
        }

    }

    public Customer getCustomerByEmail(String customerEmail) {
        CustomerEntity customerEntity = customerDao.findByCustomerEmail(customerEmail);
        if(customerEntity != null){
            return Customer.builder()
                    .customerAddress(customerEntity.getCustomerAddress())
                    .customerEmail(customerEntity.getCustomerEmail())
                    .phoneNumber(customerEntity.getPhoneNumber())
                    .lastName(customerEntity.getLastName())
                    .firstName(customerEntity.getFirstName())
                    .build();
        }else{
            throw new RuntimeException("User Not Found");
        }
    }
}
