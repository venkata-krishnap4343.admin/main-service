package com.mainservice.customer;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/customer/service")
@AllArgsConstructor
public class CustomerController {

    CustomerService customerService;

    @PostMapping(path = "/add-new-customer", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer addNewCustomer(@RequestBody Customer customer) {
        return customerService.addNewCustomer(customer);
    }

    @GetMapping(path = "/get-customer", produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer getCustomer(@RequestParam("customerEmail") String customerEmail){
        return customerService.getCustomerByEmail(customerEmail);
    }

}
