package com.mainservice.shared;

import com.mainservice.book.Book;
import com.mainservice.book.BookEntity;
import com.mainservice.customer.Customer;
import com.mainservice.customer.CustomerEntity;

import java.util.Date;

public class DataFactory {
    private DataFactory(){ }

    public static final Customer validCustomer(){
        return Customer.builder()
                .customerEmail("test@gmail.com")
                .firstName("test")
                .lastName("random")
                .phoneNumber("123456789")
                .customerAddress("123 abc xyz")
                .build();
    }

    public static final CustomerEntity validCustomerEntity(){
        return CustomerEntity.builder()
                .customerEmail("test@gmail.com")
                .firstName("test")
                .lastName("random")
                .phoneNumber("123456789")
                .customerAddress("123 abc xyz")
                .build();
    }

    public static final Book validBook(){
        return Book.builder()
                .price("10")
                .isbn("123456")
                .bookName("test")
                .stock(10)
                .author("abc")
                .build();
    }

    public static final BookEntity validBookEntity(){
        return BookEntity.builder()
                .price("10")
                .isbn("123456")
                .bookName("test")
                .stock(10)
                .author("abc")
                .build();
    }

    public static final LogMessage validLogMessage(){
        return LogMessage.builder()
                .message("test")
                .time(new Date().toString()).build();
    }
}
