package com.mainservice.messaging;

import com.mainservice.shared.LogMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.mainservice.shared.DataFactory.validLogMessage;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class BookLogMessagingServiceTest {

    @Mock
    RabbitTemplate template;

    @InjectMocks
    BookLogMessagingService bookLogMessagingService;
    @Test
    void convertAndSend() {

        //when
        String exchange = "book_log_exchange";
        String routingKey = "book_log_routing_key";
        LogMessage logMessage = validLogMessage();

        //Test
        bookLogMessagingService.convertAndSend(logMessage);
        verify(template, atLeastOnce()).convertAndSend(exchange, routingKey, logMessage);
    }
}