package com.mainservice.messaging;

import com.mainservice.shared.LogMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.mainservice.shared.DataFactory.validLogMessage;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CustomerLogMessagingServiceTest {

    @Mock
    RabbitTemplate template;

    @InjectMocks
    CustomerLogMessagingService customerLogMessagingService;

    @Test
    void convertAndSend() {

        //when
        String exchange = "customer_log_exchange";
        String routingKey = "customer_log_routing_key";
        LogMessage logMessage = validLogMessage();

        //Test
        customerLogMessagingService.convertAndSend(logMessage);
        verify(template, atLeastOnce()).convertAndSend(exchange, routingKey, logMessage);

    }
}