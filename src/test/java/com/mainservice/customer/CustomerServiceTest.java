package com.mainservice.customer;

import com.mainservice.messaging.CustomerLogMessagingService;
import com.mainservice.shared.LogMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;

import static com.mainservice.shared.DataFactory.validCustomer;
import static com.mainservice.shared.DataFactory.validCustomerEntity;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CustomerServiceTest {

    @Mock
    CustomerDao customerDao;

    @Mock
    CustomerLogMessagingService customerLogMessagingService;

    @InjectMocks
    CustomerService customerService;

    @Test
    void addNewCustomer() {
        //when
        Customer customer = validCustomer();
        CustomerEntity customerEntity = validCustomerEntity();
        when(customerDao.save(customerEntity)).thenReturn(customerEntity);
        when(customerDao.findByCustomerEmail(customerEntity.getCustomerEmail())).thenReturn(null);

        //Test
        Customer response = customerService.addNewCustomer(customer);
        verify(customerLogMessagingService).convertAndSend(LogMessage.builder().message(customerEntity.toString()).time(new Date().toString()).build());

        Assertions.assertEquals(customer, response);

    }

    @Test
    void getCustomerByEmail() {

        //when
        String customerEmail = "test@gmail.com";
        Customer customer = validCustomer();
        CustomerEntity customerEntity = validCustomerEntity();
        when(customerDao.findByCustomerEmail(customerEntity.getCustomerEmail())).thenReturn(customerEntity);

        //Test
        Customer response = customerService.getCustomerByEmail(customerEmail);

        Assertions.assertEquals(customer, response);
    }

    @Test
    void addNewCustomerExceptionCheck() {
        //when
        Customer customer = validCustomer();
        CustomerEntity customerEntity = validCustomerEntity();
        when(customerDao.findByCustomerEmail(customerEntity.getCustomerEmail())).thenReturn(customerEntity);

        //Test
        assertThrows(RuntimeException.class,
                ()-> customerService.addNewCustomer(customer));

    }

    @Test
    void getCustomerByEmailExceptionCheck() {

        //when
        String customerEmail = "test@gmail.com";
        CustomerEntity customerEntity = validCustomerEntity();
        when(customerDao.findByCustomerEmail(customerEntity.getCustomerEmail())).thenReturn(null);

        //Test
        assertThrows(RuntimeException.class,
                ()-> customerService.getCustomerByEmail(customerEmail));
    }
}