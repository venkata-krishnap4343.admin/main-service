package com.mainservice.customer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.mainservice.shared.DataFactory.validCustomer;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CustomerControllerTest {


    @Autowired
    MockMvc mockMvc;

    @MockBean
    CustomerService customerService;


    @Autowired
    ObjectMapper objectMapper;

    @Test
    void addNewCustomer() throws Exception {

        //when
        Customer customer = validCustomer();
        when(customerService.addNewCustomer(customer)).thenReturn(customer);

        //Test
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/customer/service/add-new-customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(customer)))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expected = "{\n" +
                "  \"customerEmail\": \"test@gmail.com\",\n" +
                "  \"firstName\": \"test\",\n" +
                "  \"lastName\": \"random\",\n" +
                "  \"phoneNumber\": \"123456789\",\n" +
                "  \"customerAddress\": \"123 abc xyz\"\n" +
                "}";

        JSONAssert.assertEquals(response, expected, JSONCompareMode.STRICT);
    }

    @Test
    void getCustomer() throws Exception {

        //when
        String customerEmail = "test@gmail.com";
        Customer customer = validCustomer();
        when(customerService.getCustomerByEmail(customerEmail)).thenReturn(customer);

        //Test
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/customer/service/get-customer")
                .param("customerEmail", customerEmail))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expected = "{\n" +
                "  \"customerEmail\": \"test@gmail.com\",\n" +
                "  \"firstName\": \"test\",\n" +
                "  \"lastName\": \"random\",\n" +
                "  \"phoneNumber\": \"123456789\",\n" +
                "  \"customerAddress\": \"123 abc xyz\"\n" +
                "}";

        JSONAssert.assertEquals(response, expected, JSONCompareMode.STRICT);
    }
}