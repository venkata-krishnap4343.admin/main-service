package com.mainservice.book;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static com.mainservice.shared.DataFactory.validBook;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class BookControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookService bookService;


    @Autowired
    ObjectMapper objectMapper;

    @Test
    void addNewBook() throws Exception {
        //when
        Book book = validBook();
        when(bookService.addNewBook(book)).thenReturn(book);

        //Test
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/book/service/add-new-book")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expected = "{\n" +
                "  \"bookName\": \"test\",\n" +
                "  \"isbn\": \"123456\",\n" +
                "  \"author\": \"abc\",\n" +
                "  \"price\": \"10\",\n" +
                "  \"stock\": 10\n" +
                "}";

        JSONAssert.assertEquals(response, expected, JSONCompareMode.STRICT);

    }

    @Test
    void getBookWithBookName() throws Exception {
        //when
        String bookName = "test";
        Book book = validBook();
        when(bookService.getBookWithBookName(bookName)).thenReturn(book);

        //Test
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/book/service/bookName/get-book")
                .param("bookName", bookName))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expected = "{\n" +
                "  \"bookName\": \"test\",\n" +
                "  \"isbn\": \"123456\",\n" +
                "  \"author\": \"abc\",\n" +
                "  \"price\": \"10\",\n" +
                "  \"stock\": 10\n" +
                "}";
        JSONAssert.assertEquals(response, expected, JSONCompareMode.STRICT);
    }

    @Test
    void getBooksWithAuthorName() throws Exception {

        //when
        String authorName = "abc";
        List<Book> bookList = singletonList(validBook());
        when(bookService.getBooksWithAuthorName(authorName)).thenReturn(bookList);

        //Test
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/book/service/author/get-book")
                .param("author", authorName))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();

        String expected = "[\n"+
                "{\n" +
                "  \"bookName\": \"test\",\n" +
                "  \"isbn\": \"123456\",\n" +
                "  \"author\": \"abc\",\n" +
                "  \"price\": \"10\",\n" +
                "  \"stock\": 10\n" +
                "}\n" +
                "]";

        JSONAssert.assertEquals(expected, response, JSONCompareMode.STRICT);

    }
}