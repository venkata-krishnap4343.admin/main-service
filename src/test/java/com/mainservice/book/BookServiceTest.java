package com.mainservice.book;

import com.mainservice.messaging.BookLogMessagingService;
import com.mainservice.shared.LogMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.List;

import static com.mainservice.shared.DataFactory.validBook;
import static com.mainservice.shared.DataFactory.validBookEntity;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@SpringBootTest
class BookServiceTest {

    @Mock
    BookDao bookDao;

    @Mock
    BookLogMessagingService bookLogMessagingService;

    @InjectMocks
    BookService bookService;

    @Test
    void addNewBook() {

        //when
        Book book = validBook();
        BookEntity bookEntity = validBookEntity();
        when(bookDao.save(bookEntity)).thenReturn(bookEntity);
        when(bookDao.findByIsbn(bookEntity.getIsbn())).thenReturn(null);

        //Given
        Book response = bookService.addNewBook(book);
        verify(bookLogMessagingService).convertAndSend(LogMessage.builder().message(bookEntity.toString()).time(new Date().toString()).build());
        Assertions.assertEquals(book, response);
    }

    @Test
    void getBookWithBookName() {
        //when
        String bookName = "test";
        Book book = validBook();
        BookEntity bookEntity = validBookEntity();
        when(bookDao.findByBookName(bookName)).thenReturn(bookEntity);

        //Test
        Book response = bookService.getBookWithBookName(bookName);
        Assertions.assertEquals(book, response);

    }

    @Test
    void getBooksWithAuthorName() {

        //when
        String author = "abc";
        List<Book> book = singletonList(validBook());
        List<BookEntity> bookEntities = singletonList(validBookEntity());
        when(bookDao.findByAuthor(author)).thenReturn(bookEntities);

        //Test
        List<Book> response = bookService.getBooksWithAuthorName(author);
        Assertions.assertEquals(book, response);
    }


    @Test
    void addNewBookExceptionCheck() {

        //when
        Book book = validBook();
        BookEntity bookEntity = validBookEntity();
        when(bookDao.save(bookEntity)).thenReturn(bookEntity);
        when(bookDao.findByIsbn(bookEntity.getIsbn())).thenReturn(bookEntity);

        //Given
        assertThrows(RuntimeException.class,
                ()-> bookService.addNewBook(book));
    }


    @Test
    void getBookWithBookNameExceptionCheck() {
        //when
        String bookName = "test";
        when(bookDao.findByBookName(bookName)).thenReturn(null);
        //Test
        assertThrows(RuntimeException.class,
                ()-> bookService.getBookWithBookName(bookName));

    }

    @Test
    void getBooksWithAuthorNameExceptionCheck() {

        //when
        String author = "abc";
        when(bookDao.findByAuthor(author)).thenReturn(null);

        //Test
        assertThrows(RuntimeException.class,
                ()-> bookService.getBooksWithAuthorName(author));
    }


}
